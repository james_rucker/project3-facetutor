package edu.westga.project3;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * FaceTutorApplication extends the JavaFX Application class to build the GUI
 * and start program execution.
 * 
 * @author jrucker2
 * @version Spring, 2015
 */
public class FaceTutorApplication extends Application {

	private static final String WINDOW_TITLE = "Face Tutor";
	private static final String GUI_FXML = "view/FaceTutorGui.fxml";

	/**
	 * Constructs a new FaceTutorApplication object that extends the
	 * Application class to build the GUI and start program execution.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute.
	 */
	public FaceTutorApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();

		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}

	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
