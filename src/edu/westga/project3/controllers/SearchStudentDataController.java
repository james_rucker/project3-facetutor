package edu.westga.project3.controllers;

import edu.westga.project3.model.DataMap;
import edu.westga.project3.model.StudentData;

/**
 * SearchStudentDataController manages the use case of 
 * searching for a student.
 * 
 * @author jrucker2
 * @version Spring, 2015
 */
public class SearchStudentDataController {
	
	private DataMap<String, StudentData> theMap;
	
	/**
	 * Creates a new SearchStudentDataController to to manage searching
	 * for student names in the specified map.
	 * 
	 * @precondition	aMap != null
	 * @postcondition	the controller is ready to search for students
	 * 
	 * @param aMap		the map to search  for students
	 */
	public SearchStudentDataController(DataMap<String, StudentData> aMap) {
		if (aMap == null) {
			throw new IllegalArgumentException("The map was null");
		}
		
		this.theMap = aMap;
	}
	
	/**
	 * Returns the student data that was searched for or returns 
	 * a student not found message.
	 * 
	 * @precondition	studentName != null
	 * @postcondition	returns the student value of the passed in key
	 * 
	 * @param studentName	the name of the student.
	 * @return				the student searched for or search failed message
	 */
	public StudentData searchForStudent(String studentName) {
		if (studentName == null) {
			throw new IllegalArgumentException("The student name is null");
		}
		
		if (!this.theMap.contains(studentName)) {
			return new StudentData("Not found", "Not found");
		} 
		
		return this.theMap.get(studentName);
	}
	
	/**
	 * Returns the DataMap to which students are added.
	 * 
	 * @precondition	none
	 * @return			the map
	 */
	public DataMap<String, StudentData> getMap() {
		return this.theMap;
	}
}





