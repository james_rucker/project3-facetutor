package edu.westga.project3.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.project3.datatier.TextFileLoader;
import edu.westga.project3.datatier.TextLoader;
import edu.westga.project3.model.DataMap;
import edu.westga.project3.model.StudentData;

/**
 * LoadStudentDataController manages the "load name data from file" use case:
 * reading name data from a text file and storing it in the data map.
 * 
 * @author jrucker2
 * @version Spring, 2015
 */
public class LoadStudentDataController {

	private DataMap<String, StudentData> theMap;
	private File inputFile;

	/**
	 * Creates a new LoadStudentDataController to manage adding names to the specified
	 * map.
	 * 
	 * @precondition inputFile != null && aMap != null
	 * @postcondition the controller is ready to load names
	 * 
	 * @param inputFile
	 *            the file to read
	 * @param aMap
	 *            the data map to which name will be added
	 */
	public LoadStudentDataController(File inputFile, DataMap<String, StudentData> aMap) {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		if (aMap == null) {
			throw new IllegalArgumentException("The map was null");
		}

		this.inputFile = inputFile;
		this.theMap = aMap;
	}

	/**
	 * Loads name data from the input file into the map.
	 * 
	 * @precondition none
	 * @postcondition the map contains the name data
	 * 
	 * @throws IOException
	 *             if the file cannot be read
	 */
	public void loadStudentData() throws IOException {
		TextLoader loader = new TextFileLoader(this.inputFile);
		List<String> linesFromFile = loader.loadText();

		for (String oneLineFromFile : linesFromFile) {
			this.addName(oneLineFromFile);
		}
	}

	private void addName(String oneLineFromFile) {
		String[] studentDataArray = oneLineFromFile.split("\\|");
		StudentData data = new StudentData(studentDataArray[0],
									   	studentDataArray[1]);
		this.theMap.add(data.getName(), data);
	}
}
