package edu.westga.project3.model;

/**
 * StudentData objects represent the name and pictureLink
 * 
 * @author jrucker2
 * @version	Spring, 2015
 */
public class StudentData {

	private final String name;
	private final String pictureLink;

	/**
	 * Creates a new StudentData object with the specified
	 * name, pictureLink
	 * 
	 * @precondition 	name != null && name.length() > 0 &&
	 * 				    pictureLink != null && pictureLink.length() > 0
	 * @postcondition
	 * 
	 * @param name			the name of the student
	 * @param pictureLink		the URL of the picture
	 */
	public StudentData(String name, String pictureLink) {
		if (name == null) {
			throw new IllegalArgumentException("Name is null");
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("Name is empty");
		}
		
		this.name = name;
		this.pictureLink = pictureLink;
	}

	/**
	 * Returns the name of the student.
	 * 
	 * @precondition	none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the URL of the student image.
	 * 
	 * @precondition	none
	 * @return the picture URL
	 */
	public String getPictureLink() {
		return this.pictureLink;
	}
	
}