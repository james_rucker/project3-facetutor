package edu.westga.project3.view;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FaceTutorGuiCodeBehind defines the code-behind "controller" for
 * FaceTutorGui.fxml.
 * 
 * @author jrucker2
 * @version Spring, 2015
 */
public class FaceTutorGuiCodeBehind {

	private static final String LOAD_STUDENT_DATA = "Load student data";
	private static final String SUCCESSFUL_LOAD_MESSAGE = "Successfully loaded student data";
	private static final String READ_DATA_ERROR_MESSAGE = "ERROR: couldn't load the student data";
	private static final String ERROR_DIALOG_TITLE = "Data import error";

	private FaceTutorViewModel theViewModel;

	@FXML
	private TextField selectedStudentName;

	@FXML
	private TextField selectedStudentPictureURL;

	@FXML
	private MenuItem loadStudentsMenuItem;

	@FXML
	private Button nextStudentButton;

	@FXML
	private MenuItem exitMenuItem;

	@FXML
	private ListView<String> studentsListView;

	@FXML
	private ImageView studentImage;

	/**
	 * Creates a new FaceTutorGuiCodeBehind object and its view model.
	 * 
	 * @precondition none
	 * @postcondition the object and its view model are ready to be initialized
	 */
	public FaceTutorGuiCodeBehind() {
		this.theViewModel = new FaceTutorViewModel();
	}

	/**
	 * Initializes the GUI component, binding them to the view model properties
	 * and setting their event handlers.
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setEventActions();
	}

	private void setEventActions() {
		this.exitMenuItem.setOnAction(event -> Platform.exit());
		this.loadStudentsMenuItem.setOnAction(event -> this.handleLoadAction());
		this.studentsListView
						.getSelectionModel()
						.selectedItemProperty()
						.addListener(
						(observable, oldValue, newValue) -> this
								.handleListViewSelection(newValue));
		this.nextStudentButton.setOnAction(event -> this.handleNextSelection());

	}

	private void bindComponentsToViewModel() {
		this.studentImage.imageProperty().bindBidirectional(
						this.theViewModel.posterImageProperty());
		this.selectedStudentName.textProperty().bindBidirectional(
						this.theViewModel.selectedStudentStringProperty());
		this.selectedStudentPictureURL.textProperty().bindBidirectional(
						this.theViewModel.pictureUrlStringProperty());
		this.studentsListView.setItems(this.theViewModel.getNames());

	}

	private void handleNextSelection() {
		int currentIndex = this.studentsListView.getSelectionModel()
						.getSelectedIndex();
		if (currentIndex == this.theViewModel.getNames().size() - 1) {
			this.studentsListView.getSelectionModel().selectFirst();
		} else {
			this.studentsListView.getSelectionModel().select(currentIndex + 1);
		}

	}

	private void handleListViewSelection(String newValue) {
		this.selectedStudentName.textProperty().setValue(newValue);
		this.theViewModel.searchForStudent();
	}

	private void handleLoadAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}

		try {
			this.theViewModel.loadMovieDataFrom(inputFile);
			JOptionPane.showMessageDialog(null, SUCCESSFUL_LOAD_MESSAGE,
							LOAD_STUDENT_DATA, JOptionPane.ERROR_MESSAGE);

		} catch (IOException readException) {
			JOptionPane.showMessageDialog(null, READ_DATA_ERROR_MESSAGE,
							ERROR_DIALOG_TITLE, JOptionPane.ERROR_MESSAGE);
		}
	}

	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("Text Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}

}
