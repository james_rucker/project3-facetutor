package edu.westga.project3.view;

import java.io.File;
import java.io.IOException;

import edu.westga.project3.controllers.LoadStudentDataController;
import edu.westga.project3.controllers.SearchStudentDataController;
import edu.westga.project3.model.DataMap;
import edu.westga.project3.model.StudentData;
import edu.westga.project3.model.StudentDataMap;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

/**
 * FaceTutorViewModel mediates between the view and the rest of the program.
 * 
 * @author jrucker2
 * @version Spring 2015
 *
 */
public class FaceTutorViewModel {
	
	private static final String DEFAULT_IMAGE = "Blank-person-photo.png";
	private StringProperty selectedStudent;
	private StringProperty pictureUrl;
	private ObjectProperty<Image> studentImage;
	private ObservableList<String> studentNames;
	private DataMap<String, StudentData> theMap;
	private LoadStudentDataController loadController;
	private SearchStudentDataController searchController;
	
	/**
	 * Creates a new FaceTutorViewModel object with its properties initialized.
	 * 
	 * @precondition none
	 * @postcondition the object exists
	 */
	public FaceTutorViewModel() {
		Image initialImage = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
		this.selectedStudent = new SimpleStringProperty();
		this.pictureUrl = new SimpleStringProperty();
		this.studentImage = new SimpleObjectProperty<Image>(initialImage);
		this.studentNames = FXCollections.observableArrayList();
		this.theMap = new StudentDataMap();
		this.searchController = new SearchStudentDataController(this.theMap);
	}
	
	/**
	 * Returns the property that wraps the student's image.
	 * 
	 * @precondition none
	 * @return the student image
	 */
	public Property<Image> posterImageProperty() {
		return this.studentImage;
		
	}
	
	/**
	 * Returns the property that wraps the student's name.
	 * 
	 * @precondition none
	 * @return the student name string property
	 */
	public StringProperty selectedStudentStringProperty() {
		return this.selectedStudent;
	}
	
	/**
	 * Returns the property that wraps the student picture URL.
	 * 
	 * @precondition none
	 * @return the picture URL string property
	 */
	public StringProperty pictureUrlStringProperty() {
		return this.pictureUrl;
	}
	
	/**
	 * Returns the list of the students names in the model.
	 * 
	 * @precondition none
	 * @return		 the student names
	 */
	public ObservableList<String> getNames() {
		return this.studentNames;
	}
	
	/**
	 * Returns the number of students that have been added to 
	 * this view model's collection.
	 * 
	 * @precondition none
	 * @return		 how many students
	 */
	public int size() {
		return this.theMap.size();
	}
	
	/**
	 * Tells the LoadStudentDataController to import student data from the
	 * specified file. 
	 * 
	 * @precondition	inputFile != null
	 * @postcondition	the data has been loaded, so that 
	 * 					size() equals the number of lines in the file
	 * 
	 * @param inputFile	the file to read
	 * @throws IOException 	if the file cannot be read
	 */
	public void loadMovieDataFrom(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.loadController = new LoadStudentDataController(inputFile, this.theMap);
		this.loadController.loadStudentData();
		
		this.updateNameList();
	}
	
	/**
	 * searches theMap for the studentData
	 * 
	 * @precondition	doesn't error out if the selectedstudent field is empty
	 * @postcondition	the student data is found
	 */
	public void searchForStudent() {
		if (this.selectedStudent.getValue().isEmpty()) {
			return;
		}
		
		StudentData theStudent = this.searchController.
								searchForStudent(this.selectedStudent.getValue());
	
		this.selectedStudent.set(theStudent.getName());
		this.pictureUrl.set(theStudent.getPictureLink());
		
		this.setPosterImagePropertyFor(theStudent);
	}
	
	private void setPosterImagePropertyFor(StudentData theStudent) {
		Image thePicture = null; 
		try {
			thePicture = new Image(theStudent.getPictureLink());
		} catch (Exception invalidUrl) {
			thePicture = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
		}
		this.studentImage.setValue(thePicture);
	}
	
	private void updateNameList() {
		this.studentNames.clear();
		this.studentNames.addAll(this.theMap.keys());
		this.studentNames.sort(null);
	}
	

}
