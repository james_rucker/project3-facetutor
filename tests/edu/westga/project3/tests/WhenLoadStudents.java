package edu.westga.project3.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.project3.controllers.LoadStudentDataController;
import edu.westga.project3.model.DataMap;
import edu.westga.project3.model.StudentData;
import edu.westga.project3.model.StudentDataMap;

/**
* WhenLoadMovieData defines test cases for 
* the "load movie data from file" use case.
* 
* @author cs 1302
* @version Spring, 2015
*/
public class WhenLoadStudents {
	
	private LoadStudentDataController testController;
	private File testFile;
	private DataMap<String, StudentData> theMap;
	
	/**
	 * Creates a new WhenAddAMovie object.
	 */
	public WhenLoadStudents() {
		// no-op
	}
	
	/**
	 * Set up method for the test cases.
	 * @throws IOException if test file can't be read
	 */
	@Before
	public void setUp() throws IOException {
		this.theMap = new StudentDataMap();
		this.testFile = new File(getClass().getResource("resources/names.txt").getFile());
		this.testController = new LoadStudentDataController(this.testFile, this.theMap);
		this.testController.loadStudentData();
	}

	/**
	 * Test case for loading student data.
	 */
	@Test
	public void shouldLoad7Students() {
		assertEquals(7, this.theMap.size());
	}
	
	/**
	 * Test case for loading student data.
	 */
	@Test
	public void shouldLoadFirstStudentInFile() {
		assertTrue(this.theMap.contains("Rubble, Barney"));
	}
	
	/**
	 * Test case for loading student data.
	 */
	@Test
	public void shouldLoadMiddleStudentInFile() {
		assertTrue(this.theMap.contains("Flintstone, Wilma"));
	}
	
	/**
	 * Test case for loading student data.
	 */
	@Test
	public void shouldLoadLastStudentInFile() {
		assertTrue(this.theMap.contains("Flintstone, Dino"));
	}

}
