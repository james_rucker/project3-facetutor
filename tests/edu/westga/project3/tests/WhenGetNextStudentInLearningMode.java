package edu.westga.project3.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.project3.controllers.LoadStudentDataController;
import edu.westga.project3.model.DataMap;
import edu.westga.project3.model.StudentData;
import edu.westga.project3.model.StudentDataMap;

/**
 * WhenGetNextStudentInLearningMode
 * tests the "using the next button" test case.
 *
 * @author jrucker2
 * @version spring 2015
 *
 */
public class WhenGetNextStudentInLearningMode {

	private LoadStudentDataController testController;
	private File testFile;
	private DataMap<String, StudentData> theMap;
	
	/**
	 * Creates a new WhenGetNextStudentInLearningMode
	 */
	public WhenGetNextStudentInLearningMode() {
		// no-op
	}
	
	/**
	 * Set up method for the test cases.
	 * @throws IOException if test file can't be read
	 */
	@Before
	public void setUp() throws IOException {
		this.theMap = new StudentDataMap();
		this.testFile = new File(getClass().getResource("resources/names.txt").getFile());
		this.testController = new LoadStudentDataController(this.testFile, this.theMap);
		this.testController.loadStudentData();
	}

	/**
	 * Test case for assessing which student is accessed.
	 */
	@Test
	public void firstStudentAccessedShouldBeFirstStudentInList() {
		fail("Not yet implemented");
	}
	
	/**
	 * Test case for assessing which student is accessed.
	 */
	@Test
	public void secondStudentAccessedShouldBeFirstStudentInList() {
		fail("Not yet implemented");
	}
	
	/**
	 * Test case for assessing which student is accessed.
	 */
	@Test
	public void eighthStudentAccessedShouldBeFirstStudentInTheList() {
		fail("Not yet implemented");
	}
}