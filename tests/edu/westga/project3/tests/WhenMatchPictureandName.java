package edu.westga.project3.tests;

import static org.junit.Assert.*;

import org.junit.Test;


/**
* WhenMatchPictureandName defines test cases for 
* the "matching of picture and names" use case.
* 
* @author cs 1302
* @version Spring, 2015
 */
public class WhenMatchPictureandName {
	
	
	/**
	 * Creates a new WhenMatchPictureandName
	 */
	public WhenMatchPictureandName() {
		//no-op
	}

	/**
	 * Test case for confirming the relation of a name and picture
	 */
	@Test
	public void shouldMatchIfNamesTheSame() {
		fail("Not yet implemented");
	}
	
	/**
	 * Test case for confirming the relation of a name and picture
	 */
	@Test
	public void shouldNotMatchIfNamesAreDifferent() {
		fail("Not yet implemented");
	}

}
